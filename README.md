

# FLASKAPI - A Flask API for image uploading with thumbnails
## MS IO project - Python Data Intensive course

A minimal API developed with [Flask](http://flask.pocoo.org/) framework for uploading images on a server

The main purpose of this project is to learn how to implement the essential elements in making an API :

- URL Building

- routes

- Calling API with methods GET, POST, PUT, DELETE

- Error Handling

- Testing vith pytest and test coverage with pytest-cov 

- beautiful code with pylint/flask8 and black and isort

- Interacting with Database (SQLite for storing images metadata)

- TO DO LATER : Authentication with Sessions


## How to Run

- Step 1: `git clone https://gitlab.com/datazzz/flaskapi.git`

- Step 2: Create a virtual environment `python -m venv flaskapi`

- Step 3: `cd flaskapi | source bin/activate`

- Step 4: Install the requirements: `pip install -r requirements.txt`

- Step 5: run the app `python main.py`

Uploaded Images will be stored in directory "uploads"
Thumbnails will be stored in directory "uploads/thumbnails"




## How to call the API :

  # GET : 
  
    curl -v -X GET http://127.0.0.1:5000/images/1
            

    * If an image with the given ID exists : 

        * HTTP/1.0 200 OK

      {
          "id": 1,
          "name": "c88dc8ddc76f2beae169facbd26b7ee0.jpg",
          "nb_users": 1,
          "size": 5290,
          "file_extension": "jpg",
          "image_fullpath": "uploads/c88dc8ddc76f2beae169facbd26b7ee0.jpg",
          "thumb_fullpath": "uploads/thumbnails/tn_c88dc8ddc76f2beae169facbd26b7ee0.jpg",
          "storage_date": "2021-01-31 19:35:52.504511"
      }


    * If no image with the given ID  : 
    
        {" message" : "Error 404 - No image in database for that id" }

  # POST (data+file) :
    
    curl -v -X POST -F file=@'/home/vince/Pictures/dog.jpg' http://127.0.0.1:5000/images/3

    * If an image with the given ID exists : 

        HTTP/1.0 409 CONFLICT
        { "message": "Error 409 - Image id is already used in database for another image..." } 
    
    * If no image with the given ID  : 

      {
          "id": 3,
          "name": "c88dc8ddc76f2beae169facbd26b7ee0.jpg",
          "nb_users": 1,
          "size": 5290,
          "file_extension": "jpg",
          "image_fullpath": "uploads/c88dc8ddc76f2beae169facbd26b7ee0.jpg",
          "thumb_fullpath": "uploads/thumbnails/tn_c88dc8ddc76f2beae169facbd26b7ee0.jpg",
          "storage_date": "2021-01-31 19:35:52.504511"
      }



  # DELETE : 
  
    curl -v -X DELETE http://127.0.0.1:5000/images/2
 
    * If an image with the given ID exists : 

      {"message" : "Error 204 - resource with ID xxx deleted successfully - NO CONTENT returned"}

    * If no image with the given ID  : 

        HTTP/1.0 404 NOT FOUND
        
        { "message": "Error 404 - No resource with id XXX then cannot delete it" }


  # GET : to get the thumbnail of a photo (option -o -s mon_fichier.jpg is for saving the downloaded binary file on your personal drive)
  
    curl -v -X GET http://127.0.0.1:5000/thumbnails/50 -s -o nom_fichier
            


## Use Cases

The main reason to use this API is to upload image files on a server. For security reasons, only files with extensions PNG, JPG/JPEG or GIF are allowed.


## Authorization

At the moment, API requests dont require to be authentificated

Maybe an authentification through OAuth2 will be available next month (for "fil rouge" project)

## Database structure

Metadata of uplaoded images are stored on a Sqlite database

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `id` | `int` | Unique identifier |
| `name` | `string` | Name of the image file |
| `nb_users` | `int` | The number of times a SAME image has been uploaded |
| `size` | `int` | The size of the image in bytes |

A sample database flaskapi.md is available on this GIT repository but you create your own database with the following SQL script


CREATE TABLE image_model (
id	INTEGER NOT NULL,
name	VARCHAR ( 100 ) NOT NULL,
nb_users	INTEGER NOT NULL,
size	INTEGER NOT NULL,
file_extension	VARCHAR ( 100 ) NOT NULL,
image_fullpath	VARCHAR ( 100 ) NOT NULL,
thumb_fullpath	VARCHAR ( 100 ) NOT NULL,
storage_date	VARCHAR ( 100 ) NOT NULL,
PRIMARY KEY(id)
);



## Responses

Most API endpoints return the JSON representation of the image resource created or edited. 

```javascript
{
  "name" : string,
  "nb_users" : bool,
  "size"    : string
}
```

The `message` attribute contains a message commonly used to indicate errors or, in the case of deleting a resource, success that the resource was properly deleted.

The `success` attribute describes if the transaction was successful or not.

The `data` attribute contains any other metadata associated with the response. This will be an escaped string containing JSON data.

## Status Codes

FlaskAPI returns the following status codes in its API:

| Status Code | Description |
| :--- | :--- |
| 200 | `OK` |
| 201 | `CREATED` |
| 204 | `DELETED` |
| 400 | `BAD REQUEST` |
| 404 | `NOT FOUND` |
| 500 | `INTERNAL SERVER ERROR` |
