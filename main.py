import hashlib  # no need to install
import logging
import os
from datetime import datetime

import PIL.Image
import werkzeug
from flask import Flask, send_file
from flask_restful import Api, Resource, abort, fields, marshal_with, reqparse
from flask_sqlalchemy import SQLAlchemy
from werkzeug.utils import secure_filename

app = Flask(__name__)

api = Api(app)


# Logger
logger = logging.getLogger("dev")
logger.setLevel(logging.INFO)

fileHandler = logging.FileHandler("flaskapi.log")
fileHandler.setLevel(logging.INFO)

consoleHandler = logging.StreamHandler()
consoleHandler.setLevel(logging.INFO)

logger.addHandler(fileHandler)
logger.addHandler(consoleHandler)
logger.info("Launching Flask application FLASKAPI")


app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///flaskapi.db"
db = SQLAlchemy(app)


# path to the upload directory
app.config["UPLOAD_FOLDER"] = "uploads/"
app.config["THUMBNAILS_FOLDER"] = "uploads/thumbnails/"
# Only files with following extensions are allowed
app.config["ALLOWED_EXTENSIONS"] = {"png", "jpg", "jpeg", "gif"}
app.config["THUMBNAIL_MAX_SIZE"] = 150


def createDirectory(dir):
    """create one directory if it doesn't exist

    Parameters:
    filename (string): directories (and subdirectories) to create

    Returns:
    the creation of the directory (and nested subdirectories)

    """

    if not os.path.isdir(dir):
        os.makedirs(dir)

# Create (if they dont exist) 2 nested directories for storing images and thumbnails
createDirectory(app.config["UPLOAD_FOLDER"])
createDirectory(app.config["THUMBNAILS_FOLDER"])



def extension(filename):
    """Get the extension of a filename

    Parameters:
    filename (string): Name of the file

    Returns:
    string : the substring between the LAST dot in the filename, and the end of the filename

    """

    return "." in filename and (filename.rsplit(".", 1)[1]).lower()


def allowed_file(filename):
    """Check if the extension of a file is allowed. Test is non-sensitive (pNg is allowed)

    Parameters:
    filename (string): Name of the file

    Returns:
    boolean : True if the filename contains a dot, and if

    """

    return extension(filename) in app.config["ALLOWED_EXTENSIONS"]
    # filename must contains a dot, then we extract the extension


# Find MD5 hash value of a file, to use it as a unique identifier
def file_id(filename):
    """Get the MD5 hash of a filename

    Parameters:
    filename (string): Name of the file

    Returns:
    string : MD5 hash (will be used as physical name to store the file on server)

    """
    # likely to fail with huges file
    logger.info(
        "Calculating MD5 hash for filename "
        + os.path.join(app.config["UPLOAD_FOLDER"], filename)
    )
    with open(os.path.join(app.config["UPLOAD_FOLDER"], filename), "rb") as f:
        bytes = f.read()  # read file as bytes
        readable_hash = hashlib.md5(bytes).hexdigest()
        return readable_hash
    #
    # Works with very large files
    # md5_hash = hashlib.md5()
    # with open(filename,"rb") as f:
    #    # Read and update hash in chunks of 4K
    #    for byte_block in iter(lambda: f.read(4096),b""):
    #        md5_hash.update(byte_block)
    #    return(md5_hash.hexdigest())


def thumbnails(filename):
    """Generate thumbnails

    Parameters:
    filename (string): Name of the image file to reduce

    Returns:
    an image whose max length and height is lower than app.config["THUMBNAIL_MAX_SIZE"]

    """
    try:
        image = PIL.Image.open(
            os.path.join(app.config["UPLOAD_FOLDER"], filename)
        )  # PIL.Image.open because Image.open cannot work because Image is another class
        MAX_SIZE = (app.config["THUMBNAIL_MAX_SIZE"], app.config["THUMBNAIL_MAX_SIZE"])
        image.thumbnail(MAX_SIZE)
        logger.info(
            "Saving thumbnail" + app.config["THUMBNAILS_FOLDER"] + "tn_" + filename
        )
        image.save(app.config["THUMBNAILS_FOLDER"] + "tn_" + filename)
        image1 = PIL.Image.open(app.config["THUMBNAILS_FOLDER"] + filename)
        image1.show()
    except IOError:
        logger.info(
            "Error when trying to generate thumbnail for"  + filename
        )
        pass


class ImageModel(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    nb_users = db.Column(db.Integer, nullable=False)
    size = db.Column(db.Integer, nullable=False)
    file_extension = db.Column(db.String(100), nullable=True)
    image_fullpath = db.Column(db.String(100), nullable=True)
    thumb_fullpath = db.Column(db.String(100), nullable=True)
    storage_date = db.Column(db.String(100), nullable=True)

    def __repr__(self):
        return f"Image(name = {name}, nb_users = {nb_users}, size = {size}, \
        file_extension = {file_extension}, image_fullpath = {image_fullpath}, \
        thumb_fullpath = {thumb_fullpath}, storage_date = {storage_date})"


resource_fields = {
    "id": fields.Integer,
    "name": fields.String,
    "nb_users": fields.Integer,
    "size": fields.Integer,
    "file_extension": fields.String,
    "image_fullpath": fields.String,
    "thumb_fullpath": fields.String,
    "storage_date": fields.String,
}


image_put_args = reqparse.RequestParser()
image_put_args.add_argument(
    "name", type=str, help="Name of the image is required", required=0
)
image_put_args.add_argument(
    "nb_users",
    type=int,
    help="Number of users that have uploaded that SAME image",
    required=0,
)
image_put_args.add_argument(
    "size", type=int, help="Size of the image in bytes", required=0
)

image_update_args = reqparse.RequestParser()
image_update_args.add_argument("name", type=str, help="Name of the image")
image_update_args.add_argument(
    "nb_users", type=int, help="Number of users that have uploaded that SAME image"
)
image_update_args.add_argument("size", type=int, help="Size of the image in bytes")

image_post_args = reqparse.RequestParser()
image_post_args.add_argument(
    "file", type=werkzeug.datastructures.FileStorage, location="files"
)


class Image(Resource):
    """class for Image resources"""
    @marshal_with(resource_fields)
    @classmethod
    def get(self, image_id):
        """GET method for API"""
        result = ImageModel.query.filter_by(id=image_id).first()
        if not result:
            abort(404, message="Error 404 - No image in database for that id")
        return result

    @marshal_with(resource_fields)
    @classmethod
    def put(self, image_id):
        """PUT method for API"""
        args = image_put_args.parse_args()
        result = ImageModel.query.filter_by(id=image_id).first()
        if result:
            abort(
                409,
                message="Error 409 - Image id is already used in database for another image...",
            )

        image = ImageModel(
            id=image_id, name=args["name"], nb_users=args["nb_users"], size=args["size"]
        )
        db.session.add(image)
        db.session.commit()
        return image, 201

    @marshal_with(resource_fields)
    @classmethod
    def patch(self, image_id):
        """PATCH method for API"""
        args = image_update_args.parse_args()
        result = ImageModel.query.filter_by(id=image_id).first()
        if not result:
            abort(
                404,
                message="Error 404 - No resource with ID"
                + str(image_id)
                + " then cannot update it...",
            )

        if args["name"]:
            result.name = args["name"]
        if args["nb_users"]:
            result.nb_users = args["nb_users"]
        if args["size"]:
            result.size = args["size"]

        db.session.commit()

        return result

    @marshal_with(resource_fields)
    @classmethod
    def delete(self, image_id):
        """DELETE method for API"""
        result = ImageModel.query.filter_by(id=image_id).first()
        logger.info("FOUND OBJECT TO DELETE by id " + str(image_id))
        if not result:
            logger.info("404")
            abort(
                404,
                message="Error 404 - No resource with id "
                + str(image_id)
                + " then cannot delete it",
            )
        else:
            logger.info("DELETED")
            result = ImageModel.query.filter_by(id=image_id).delete()
        db.session.commit()
        return "", 204

    @marshal_with(resource_fields)
    @classmethod
    def post(self, image_id):
        """POST method for API"""
        result = ImageModel.query.filter_by(id=image_id).first()
        if result:
            abort(
                409,
                message="Error 409 - Image id is already used in database for another image...",
            )

        # ok, we can extract the image and save it on drive
        data = image_post_args.parse_args()
        if data["file"] == "":
            return {"data": "", "message": "No file found", "status": "error"}
        photo = data["file"]
        if photo:
            source_filename = secure_filename(data["file"].filename)
            md55 = file_id(source_filename)
            filename = md55 + "." + extension(source_filename)
            photo.save(os.path.join(app.config["UPLOAD_FOLDER"], filename))
            ###useless now fullpath = os.path.join(app.config['UPLOAD_FOLDER'],filename)

            # create thumbnail and store it in sub-directory "uploads/thumbnails"
            thumbnails(filename)

            # image file is saved on drive - now write metadata in database
            name = filename
            nb_users = 1
            size = os.path.getsize(os.path.join(app.config["UPLOAD_FOLDER"], filename))
            file_extension = extension(filename)
            image_fullpath = os.path.join(app.config["UPLOAD_FOLDER"], name)
            thumb_fullpath = os.path.join(app.config["THUMBNAILS_FOLDER"], "tn_" + name)
            storage_date = datetime.now()
            image = ImageModel(
                id=image_id,
                name=name,
                nb_users=nb_users,
                size=size,
                file_extension=file_extension,
                image_fullpath=image_fullpath,
                thumb_fullpath=thumb_fullpath,
                storage_date=storage_date,
            )
            db.session.add(image)
            db.session.commit()

            result = ImageModel.query.filter_by(id=image_id).first()
            if not result:
                abort(
                    404,
                    message="Error 404 - ERROR WHEN WRITING METADATA OF NEW IMAGE IN THE DATABASE",
                )
            return result
            # return {
            #        'source name in the request : ':source_filename,
            #        'target name on the server : ':filename,
            #        'message':'Image stored on server and metadata saved in database ',
            #        'File identifier':md55
            #        }
            # return image, 201


class ImageOnly(Resource):
    def get(self, image_hash):
        filename = os.path.join(app.config["THUMBNAILS_FOLDER"], image_hash)
        try:
            return send_file(filename, attachment_filename=filename)
        except Exception as e:
            return str(e)


class Thumbnail(Resource):
    # @marshal_with(resource_fields)
    def get(self, image_id):
        """GEt method for API for reading thumbnails"""
        result = ImageModel.query.filter_by(id=image_id).first()
        if not result:
            abort(404, message="Error 404 - No thumbnail in database for that id")
        logger.info("SERVING THUMBNAIL " + result.thumb_fullpath)
        # return request.url_root+os.path.join(app.config['THUMBNAILS_FOLDER'],"tn_"+result.name)
        return send_file(result.thumb_fullpath)


api.add_resource(Image, "/images/<int:image_id>")

api.add_resource(Thumbnail, "/thumbnails/<int:image_id>")

if __name__ == "__main__":
    app.run(host="127.0.0.1", port=int("5000"), debug=True)
