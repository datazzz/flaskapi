# -*- coding: utf-8 -*-

import requests
from flask import Flask

url = 'http://127.0.0.1:8282/im_size'
my_img = {'image': open('/home/vince/Pictures/dog.jpg', 'rb')}
r = requests.post(url, files=my_img)

# convert server response into JSON format.
print("r.status_code")
print(r.status_code)
print("r.headers")
print(r.headers)
print("r.encoding")
print(r.encoding)
print("r.text")
print(r.text)
print(r.json)
print("r.json")


multiple_files = [
    ('image', ('test.jpg', open('/home/vince/Pictures/dog.jpg', 'rb'))),
    ('image', ('test.jpg', open('/home/vince/Pictures/cat.jpeg', 'rb')))
]
# simplified form
# multiple_files = [
#     ('image', open('test.jpg', 'rb')),
#     ('image', open('test.jpg', 'rb'))
# ]
r = requests.post(url, files=multiple_files)

# convert server response into JSON format.
print(r.status_code)
print(r.headers)
print(r.encoding)
print(r.text)
print(r.json)